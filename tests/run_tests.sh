#!/bin/sh

PYTHON_VERSION=$(python3 -Esc "import sys; sys.stdout.write('{0.major}.{0.minor}'.format(sys.version_info))")
err=0

if [ "$(pcs stonith list 2> /dev/null | wc -l)" -eq 0 ]; then
	echo "ERROR: pcs: no agents available..."
	err=$((err+1))
fi
echo "INFO: pcs: agents available..."

# test bundled libraries
declare -A libs=(
		# aliyun
		["aliyunsdkcore"]="sys.path.insert(0, '/usr/lib/fence-agents/support/aliyun/lib/python$PYTHON_VERSION/site-packages');"
		["aliyunsdkecs"]="sys.path.insert(0, '/usr/lib/fence-agents/support/aliyun/lib/python$PYTHON_VERSION/site-packages');"
		# aws
		["boto3"]="sys.path.insert(0, '/usr/lib/fence-agents/support/aws/lib/python$PYTHON_VERSION/site-packages');"
		# azure
		["azure"]="sys.path.insert(0, '/usr/lib/fence-agents/support/azure/lib/python$PYTHON_VERSION/site-packages');"
		["msrestazure"]="sys.path.insert(0, '/usr/lib/fence-agents/support/azure/lib/python$PYTHON_VERSION/site-packages');"
		# common
		["pexpect"]="sys.path.insert(0, '/usr/lib/fence-agents/support/common/lib/python$PYTHON_VERSION/site-packages');"
		["suds"]="sys.path.insert(0, '/usr/lib/fence-agents/support/common/lib/python$PYTHON_VERSION/site-packages');"
		# google
		["googleapiclient"]="sys.path.insert(0, '/usr/lib/fence-agents/support/google/lib/python$PYTHON_VERSION/site-packages');"
		["pyroute2"]="sys.path.insert(0, '/usr/lib/fence-agents/support/google/lib/python$PYTHON_VERSION/site-packages');"
		)

for lib in "${!libs[@]}"; do
	output=$(python3 -c "import sys; sys.path.append('/usr/share/fence'); \
		${libs[$lib]} \
		import $lib" 2>&1)
	if [ $? -ne 0 ]; then
		echo -e "ERROR: Failed to import $lib:\n$output"
		err=$((err+1))
	else
		echo "INFO: importing $lib works..."
	fi
done

if [ $err -ge 1 ]; then
	echo -e "\nERROR: $err tests FAILED..."
	exit 1
fi
